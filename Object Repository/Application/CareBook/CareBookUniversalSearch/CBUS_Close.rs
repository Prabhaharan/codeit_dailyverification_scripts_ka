<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CBUS_Close</name>
   <tag></tag>
   <elementGuidId>9b21adf3-6623-4507-a534-a97e0bd92774</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[contains(text(),&quot;Universal Search&quot;)]/..//following-sibling::button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[contains(text(),&quot;Universal Search&quot;)]/..//following-sibling::button</value>
   </webElementProperties>
</WebElementEntity>
