<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>editGoalIcon</name>
   <tag></tag>
   <elementGuidId>112d53c4-a5d0-45b9-a3f5-679c0fc89239</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//img[@title='Edit this goal']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//img[@title='Edit this goal']</value>
   </webElementProperties>
</WebElementEntity>
