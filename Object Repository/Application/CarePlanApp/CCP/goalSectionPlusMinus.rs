<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>goalSectionPlusMinus</name>
   <tag></tag>
   <elementGuidId>ca73460a-638f-4fb6-90ad-5665a5492ec1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//legend[text()='Long-Term &amp; Short-Term Goals']/following::img[@src=&quot;../images/btnMinus.gif&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//legend[text()='Long-Term &amp; Short-Term Goals']/following::img[@src=&quot;../images/btnMinus.gif&quot;])[1]</value>
   </webElementProperties>
</WebElementEntity>
