<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>showFilterIconInHomePage</name>
   <tag></tag>
   <elementGuidId>d0af57b4-091f-4224-a5a2-c18ee52faafa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[text()='Filter By:']/preceding-sibling::td/img[@onclick='showFilter();']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Filter By:']/preceding-sibling::td/img[@onclick='showFilter();']</value>
   </webElementProperties>
</WebElementEntity>
