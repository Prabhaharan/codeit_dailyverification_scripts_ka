<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_CareTeamWizard</name>
   <tag></tag>
   <elementGuidId>7655e3db-0e65-48d0-8fcc-128be9d009c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-tab-item/span[contains(text(),&quot;Care Team&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
