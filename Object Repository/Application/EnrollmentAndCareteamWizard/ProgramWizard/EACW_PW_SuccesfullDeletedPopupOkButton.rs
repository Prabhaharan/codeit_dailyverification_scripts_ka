<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_SuccesfullDeletedPopupOkButton</name>
   <tag></tag>
   <elementGuidId>78aebd0c-87ab-4bc3-9611-50dba7b5e5ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[text()='Successfully deleted the most recent program episode.']/../../following-sibling::*//span[text()='OK']</value>
   </webElementProperties>
</WebElementEntity>
