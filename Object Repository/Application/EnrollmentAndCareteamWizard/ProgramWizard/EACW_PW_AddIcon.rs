<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_AddIcon</name>
   <tag></tag>
   <elementGuidId>2dc3954b-d0a7-4c68-a0a7-ec8004e282d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@aria-label=&quot;Add a new program&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@aria-label=&quot;Add a new program&quot;]</value>
   </webElementProperties>
</WebElementEntity>
