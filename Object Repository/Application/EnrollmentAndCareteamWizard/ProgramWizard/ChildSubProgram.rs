<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ChildSubProgram</name>
   <tag></tag>
   <elementGuidId>34d3dcba-b6e8-4e40-a600-e9814517bf8e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[@class=&quot;sub-program-chip ng-binding ng-scope&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[@class=&quot;sub-program-chip ng-binding ng-scope&quot;]</value>
   </webElementProperties>
</WebElementEntity>
