<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DAA_MPP_EmergencyLastNameInputField</name>
   <tag></tag>
   <elementGuidId>22ea1460-afd7-4a85-91e2-4fe1ece1f792</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'lNameEmergencyTextItem']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>lNameEmergencyTextItem</value>
   </webElementProperties>
</WebElementEntity>
