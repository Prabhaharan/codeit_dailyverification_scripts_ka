<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OrganizationName_dropdown</name>
   <tag></tag>
   <elementGuidId>a2aabf0d-9afd-4723-8ba2-c641b85f97b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Organization Name']/following::select[1]</value>
   </webElementProperties>
</WebElementEntity>
