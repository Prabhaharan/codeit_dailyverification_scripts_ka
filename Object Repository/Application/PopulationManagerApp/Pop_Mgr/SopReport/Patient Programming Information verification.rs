<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Patient Programming Information verification</name>
   <tag></tag>
   <elementGuidId>3c9bfbd4-a7bf-4b92-b4d6-b21abe131d67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//td[text()='Patient ID'][@valign='middle']/../following-sibling::tr[1]/td)[1]</value>
   </webElementProperties>
</WebElementEntity>
