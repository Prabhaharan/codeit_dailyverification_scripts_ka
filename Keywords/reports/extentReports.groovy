package reports

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import junit.framework.TestListener
import sun.security.ssl.Alerts
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.Alert
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver

import org.apache.commons.io.FileUtils
import java.awt.Rectangle
import java.awt.Robot
import java.awt.image.BufferedImage
import java.io.IOException
import java.text.SimpleDateFormat
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.awt.Toolkit
import javax.imageio.ImageIO;




public class extentReports {
	
	@Keyword
	public static ExtentReports getInstance(){
		int extendCount=0
		ExtentReports extent
		if (extent == null) {
			Date d=new Date();
			String fileName=GlobalVariable.testSuite+d.toString().replace(":", "_").replace(" ", "_")+".html";
			extent = new ExtentReports(System.getProperty("user.dir")+"//CustomizedReports//"+fileName, true, DisplayOrder.NEWEST_FIRST);
			extent.addSystemInfo("Selenium Version", "2.53.0").addSystemInfo(
					"Environment", "QA");
		}

		return extent;
	}
	
	@Keyword
	public String print(ExtentTest test,boolean verifyStatus,String printStatement) {
		if(verifyStatus){
			test.log(LogStatus.INFO, printStatement);
			return "Pass"
		}else{
			test.log(LogStatus.FAIL, "Fail : "+printStatement);
			takeScreenshot(test)
			return "Fail"
		}
	}
	@Keyword
	public String takeScreenshot(ExtentTest test) {
		WebDriver driver = DriverFactory.getWebDriver()
		Date d = new Date();
		String screenshotFile = d.toString().replace(":", "_").replace(" ", "_")+ ".png";
		try{
			WebUI.takeScreenshot(System.getProperty("user.dir")+"\\screenshots\\" + screenshotFile)
			test.log(LogStatus.INFO, "Screenshot-> "
					+ test.addScreenCapture(System.getProperty("user.dir")+"\\screenshots\\"+ screenshotFile));
				
		}catch(Exception e){
			test.log(LogStatus.INFO, "**Unexpected Alert Pop is opened - Cannot take screenshot ");
		}
		
	}

}
