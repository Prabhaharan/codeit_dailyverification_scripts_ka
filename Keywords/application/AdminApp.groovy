package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.Map
import keywordsLibrary.CommomLibrary
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class AdminApp {
	@Keyword
	public Boolean AdminAppNavigation(){
		try{
			WebUI.verifyElementPresent(findTestObject('Application/DemographicsApp/AppAdminIcon'), 5, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/DemographicsApp/AppAdminIcon'), FailureHandling.STOP_ON_FAILURE)
			Boolean appNavigationStatus=WebUI.verifyElementPresent(findTestObject('Object Repository/Application/DemographicsApp/AA_UpdateUser_Click'), 5, FailureHandling.OPTIONAL)
			WebUI.click(findTestObject('Object Repository/Application/DemographicsApp/AA_UpdateUser_Click'), FailureHandling.STOP_ON_FAILURE)
			//Boolean appNavigationStatus=WebUI.verifyElementPresent(findTestObject('Object Repository/Application/DemographicsApp/AA_UpdateUser_Click'), 5, FailureHandling.OPTIONAL)
			return appNavigationStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean searchUserInAdminAppwithlastName(String userLastName){
		try{			
			TestObject UserObject = findTestObject('Base/commanXpath')
			UserObject.findProperty('xpath').setValue("//div[text()='" +userLastName+"']")
			Boolean userSearchStatus=WebUI.verifyElementPresent(UserObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(UserObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return userSearchStatus
		} catch(Exception e){
			return false
		}
	}


	@Keyword
	public Boolean searchPatientInAdminAppwithdifferentField(Map<String, String> verifyfields){
		CommomLibrary commomLibrary=new CommomLibrary()
		try{
			for(def fieldName : verifyfields.keySet()){
				switch(fieldName){
					case "patientID":
					//div[@id='tabappCareteams']//following::div[text()='2']
						def fieldXpath ="//div[@id='tabappCareteams']//following::div[text()='"+verifyfields.get(fieldName)+"']"

						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "lastName":
						def fieldXpath ="//div[@id='tabappCareteams']//following::div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "firstName":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
					//def fieldXpathFN ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						break
					case "middleName":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "dateOfBirth":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "gender":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "SSN":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "address1":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "address2":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
					//def fieldXpathFN ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						break
					case "city":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "state":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "zipcode":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "telephone":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "parentName":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "programName":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "programEnrollmentConsent":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
					//def fieldXpathFN ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						break
					case "patientStatus":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "insurance":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "organization":
						def fieldXpath ="//div[@id='tabappCareteams']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
				}
			}
			return true
		}catch(Exception e){
			return false
		}
	}
}

